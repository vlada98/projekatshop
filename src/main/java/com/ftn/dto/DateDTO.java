package com.ftn.dto;

public class DateDTO {

    //07/09/2019
    // mesec/dan/godina
    private String datum;

    public DateDTO() {
    }

    public DateDTO(String datum) {
        this.datum = datum;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }
}
