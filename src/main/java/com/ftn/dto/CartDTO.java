package com.ftn.dto;

import com.ftn.model.StatusOfDelivery;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CartDTO {

    private Integer id;

    private LocalDate date;

    private StatusOfDelivery status;

    private String supplierFirstLastName;

    private List<ItemDTO> items = new ArrayList<>();

    private List<Integer> quantities = new ArrayList<>();

    public CartDTO() {
    }

    public CartDTO(Integer id, LocalDate date, StatusOfDelivery status, String supplierFirstLastName) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.supplierFirstLastName = supplierFirstLastName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public StatusOfDelivery getStatus() {
        return status;
    }

    public void setStatus(StatusOfDelivery status) {
        this.status = status;
    }

    public String getSupplierFirstLastName() {
        return supplierFirstLastName;
    }

    public void setSupplierFirstLastName(String supplierFirstLastName) {
        this.supplierFirstLastName = supplierFirstLastName;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    public List<Integer> getQuantities() {
        return quantities;
    }

    public void setQuantities(List<Integer> quantities) {
        this.quantities = quantities;
    }
}
