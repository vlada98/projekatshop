package com.ftn.dto;

public class CartSupplierDTO {
    private Integer cartId;
    private Integer supplierId;
    private StatusDTO statusDTO;

    public CartSupplierDTO() {

    }

    public CartSupplierDTO(Integer cartId, Integer supplierId, StatusDTO statusDTO) {
        this.cartId = cartId;
        this.supplierId = supplierId;
        this.statusDTO = statusDTO;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public StatusDTO getStatusDTO() {
        return statusDTO;
    }

    public void setStatusDTO(StatusDTO data) {
        this.statusDTO = data;
    }
}
