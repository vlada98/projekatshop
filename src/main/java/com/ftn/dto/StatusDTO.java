package com.ftn.dto;

import com.ftn.model.StatusOfDelivery;

public class StatusDTO {
    private StatusOfDelivery data;

    public StatusDTO() {

    }

    public StatusDTO(StatusOfDelivery data) {
        this.data = data;
    }

    public StatusOfDelivery getData() {
        return data;
    }

    public void setData(StatusOfDelivery data) {
        this.data = data;
    }
}
