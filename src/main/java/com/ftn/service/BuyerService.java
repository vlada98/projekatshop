package com.ftn.service;

import com.ftn.dto.CartDTO;

import java.util.List;

public interface BuyerService {


    List<CartDTO> getAllCartsByBuyerId(Integer id);
}
