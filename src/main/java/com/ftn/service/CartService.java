package com.ftn.service;

import com.ftn.dto.CartDTO;
import com.ftn.dto.CartSupplierDTO;
import com.ftn.dto.DateDTO;
import com.ftn.model.Cart;
import com.ftn.model.StatusOfDelivery;

import java.util.List;

public interface CartService {
    void changeStatusOfCart(Integer id, StatusOfDelivery statusOfDelivery);

    void saveCart(Cart cart);

    void updateCartSetSup(CartSupplierDTO cartSupplierDTO);

    List<Cart> getAllWithoutSup();

    List<Cart> getAllByIdSup(Integer id);

    List<CartDTO> getDayReport(DateDTO dateDTO);

    List<CartDTO> getWeekReport(DateDTO dateDTO);

    List<CartDTO> getMonthReport(DateDTO dateDTO);
}
