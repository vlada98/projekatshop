package com.ftn.service;

import com.ftn.dto.ItemDTO;
import com.ftn.model.FavoriteItem;
import com.ftn.model.ItemInCart;

import java.util.List;

public interface ItemService {
    List<ItemDTO> getAll();

    List<ItemDTO> getAllFavoritesById(Integer id);

    List<ItemDTO> getAllPossiblesById(Integer id);

    List<ItemDTO> getAllInCartById(Integer id);

    void deleteFavoriteItem(FavoriteItem fi);

    FavoriteItem saveFavoriteItem(FavoriteItem fi);

    void deleteItemInCart(ItemInCart ic);

    void addItemInCart(ItemInCart ic);

    void updateItemInCart(ItemInCart ic);

    List<ItemDTO> getAllDiscount();

    void updateDisc(Integer id);

    ItemDTO saveItem(ItemDTO itemDTO);

    ItemDTO updateItem(ItemDTO itemDTO);

    void deleteItem(Integer id);
}
