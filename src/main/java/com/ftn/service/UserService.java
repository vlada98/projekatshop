package com.ftn.service;

import com.ftn.dto.UserDTO;
import com.ftn.dto.UserLoginDTO;
import com.ftn.model.User;

import java.util.List;

public interface UserService {
    UserDTO login(UserLoginDTO userLoginDTO);

    UserDTO save(UserDTO userDto);

    UserDTO update(UserDTO userDto);

    List<User> getAllBuySup();

    void changeRole(Integer id);

    void deleteSup(Integer id);

    List<User> getAllSup();
}
