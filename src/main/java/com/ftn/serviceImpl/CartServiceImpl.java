package com.ftn.serviceImpl;

import com.ftn.dto.CartDTO;
import com.ftn.dto.CartSupplierDTO;
import com.ftn.dto.DateDTO;
import com.ftn.dto.ItemDTO;
import com.ftn.model.*;
import com.ftn.repository.*;
import com.ftn.service.CartService;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartServiceImpl implements CartService {
    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private BoughtItemRepository boughtItemRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemInCartRepository itemInCartRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void changeStatusOfCart(Integer id, StatusOfDelivery statusOfDelivery) {
        System.out.println("SALJEM " + statusOfDelivery);
        cartRepository.changeStatusOfCart(id, statusOfDelivery);
    }

    @Override
    public void saveCart(Cart cart) {

        cart.setDate(LocalDate.now());
        cart.setStatus(StatusOfDelivery.BOUGHT);

        Cart snimljen = cartRepository.save(cart);

        for (BoughtItem b : cart.getBought()) {
            Item i = itemRepository.findById(b.getItem_id()).orElse(null);

            ItemInCart ic = itemInCartRepository.findByBuyer_idAndItem_id(cart.getBuyer_id(), b.getItem_id());
            itemInCartRepository.delete(ic);
            if (i == null) {
                continue;
            }
            i.setQuantity(i.getQuantity() - b.getTotalQuantity());
            itemRepository.save(i);
            b.setCart_id(snimljen.getId());
            boughtItemRepository.save(b);
        }
    }


    @Override
    public void updateCartSetSup(CartSupplierDTO cartSupplierDTO) {
        Cart cart = cartRepository.findById(cartSupplierDTO.getCartId()).orElse(null);
        cart.setSupplier_id(cartSupplierDTO.getSupplierId());
        cart.setStatus(cartSupplierDTO.getStatusDTO().getData());
        cartRepository.save(cart);
    }

    @Override
    public List<Cart> getAllWithoutSup() {
        List<Cart> all = cartRepository.findAll();
        List<Cart> res = new ArrayList<>();
        for (Cart c : all) {
            if (c.getSupplier_id() == null) {
                res.add(c);
            }
        }
        return res;
    }

    @Override
    public List<Cart> getAllByIdSup(Integer id) {
        List<Cart> all = cartRepository.findAll();
        List<Cart> res = new ArrayList<>();
        for (Cart c : all) {
            Integer supplier_id = c.getSupplier_id();
            if (supplier_id == null) {
                continue;
            }
            if (supplier_id.equals(id) && c.getStatus().equals(StatusOfDelivery.IN_PROGRESS)) {
                res.add(c);
            }
        }
        return res;
    }

    @Override
    public List<CartDTO> getDayReport(DateDTO dateDTO) {
        List<CartDTO> sveKorpe = sve();
        String[] podaci = dateDTO.getDatum().split("/");
        LocalDate t = LocalDate.of(Integer.parseInt(podaci[2]), Integer.parseInt(podaci[0]), Integer.parseInt(podaci[1]));
        System.out.println("DAILY: " + t);
        List<CartDTO> res = new ArrayList<>();

        for (CartDTO c : sveKorpe) {
            if (c.getDate().compareTo(t) == 0) {
                res.add(c);
            }
        }
        return res;
    }

    @Override
    public List<CartDTO> getWeekReport(DateDTO dateDTO) {

        List<CartDTO> sveKorpe = sve();
        String[] podaci = dateDTO.getDatum().split("/");
        LocalDate t = LocalDate.of(Integer.parseInt(podaci[2]), Integer.parseInt(podaci[0]), Integer.parseInt(podaci[1]));
        LocalDate pocetakNedelje = t.with(DayOfWeek.MONDAY);
        LocalDate krajNedelje = pocetakNedelje.plusDays(7);


        List<CartDTO> res = new ArrayList<>();

        for (CartDTO c : sveKorpe) {
            if (c.getDate().plusDays(1).isAfter(pocetakNedelje) && c.getDate().minusDays(1).isBefore(krajNedelje)) {
                res.add(c);
            }
        }
        return res;


    }

    @Override
    public List<CartDTO> getMonthReport(DateDTO dateDTO) {
        List<CartDTO> sveKorpe = sve();
        String[] podaci = dateDTO.getDatum().split("/");
        LocalDate t = LocalDate.of(Integer.parseInt(podaci[2]), Integer.parseInt(podaci[0]), 1);
        List<CartDTO> res = new ArrayList<>();

        for (CartDTO c : sveKorpe) {
            if (c.getDate().plusDays(1).isAfter(t) ) {
                res.add(c);
            }
        }
        return res;
    }

    public List<CartDTO> sve() {
        List<Cart> carts = cartRepository.findAll();
        List<CartDTO> res = new ArrayList<>();
        for (Cart c : carts) {
            if (c.getStatus().equals(StatusOfDelivery.BOUGHT) || c.getStatus().equals(StatusOfDelivery.IN_PROGRESS)) {
                continue;
            }
            CartDTO cartDTO = modelMapper.map(c, CartDTO.class);
            for (BoughtItem bi : c.getBought()) {
                cartDTO.getItems().add(modelMapper.map(itemRepository.findById(bi.getItem_id()).orElse(null), ItemDTO.class));
                cartDTO.getQuantities().add(bi.getTotalQuantity());
                Integer supplier_id = c.getSupplier_id();
                if (supplier_id == null) {
                    continue;
                }
                User user = userRepository.findById(supplier_id).orElse(null);
                cartDTO.setSupplierFirstLastName(user.getFirstName() + " " + user.getLastName());
            }
            res.add(cartDTO);
        }
        return res;
    }
}
