package com.ftn.serviceImpl;

import com.ftn.dto.CartDTO;
import com.ftn.dto.ItemDTO;
import com.ftn.model.BoughtItem;
import com.ftn.model.Cart;
import com.ftn.model.User;
import com.ftn.repository.CartRepository;
import com.ftn.repository.ItemRepository;
import com.ftn.repository.UserRepository;
import com.ftn.service.BuyerService;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BuyerServiceImpl implements BuyerService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<CartDTO> getAllCartsByBuyerId(Integer id) {
        List<Cart> carts = cartRepository.findByBuyer_id(id);
        List<CartDTO> res = new ArrayList<>();
        for (Cart c : carts) {
            CartDTO cartDTO = modelMapper.map(c, CartDTO.class);
            for (BoughtItem bi : c.getBought()) {
                cartDTO.getItems().add(modelMapper.map(itemRepository.findById(bi.getItem_id()).orElse(null), ItemDTO.class));
                cartDTO.getQuantities().add(bi.getTotalQuantity());
                Integer supplier_id = c.getSupplier_id();
                if (supplier_id == null) {
                    continue;
                }
                User user = userRepository.findById(supplier_id).orElse(null);
                cartDTO.setSupplierFirstLastName(user.getFirstName() + " " + user.getLastName());
            }
            res.add(cartDTO);
        }
        return res;
    }
}
