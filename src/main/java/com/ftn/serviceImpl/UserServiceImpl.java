package com.ftn.serviceImpl;

import com.ftn.dto.UserDTO;
import com.ftn.dto.UserLoginDTO;
import com.ftn.model.*;
import com.ftn.repository.*;
import com.ftn.service.UserService;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoughtItemRepository boughtItemRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private FavoriteItemRepository favoriteItemRepository;

    @Autowired
    private ItemInCartRepository itemInCartRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserDTO login(UserLoginDTO userLoginDTO) {
        List<User> users = userRepository.findAll();
        for (User u : users) {
            if (u.isDeleted()) {
                continue;
            }
            if (u.getUsername().equals(userLoginDTO.getUsername()) && u.getPassword().equals(userLoginDTO.getPassword())) {
                return modelMapper.map(u, UserDTO.class);
            }
        }
        return null;
    }

    @Override
    public UserDTO save(UserDTO userDto) {
        User user;
        if (userDto.getRole().equals(Role.BUYER)) {
            user = userRepository.save(modelMapper.map(userDto, Buyer.class));
        } else if (userDto.getRole().equals(Role.ADMIN)) {
            user = userRepository.save(modelMapper.map(userDto, Admin.class));
        } else {
            user = userRepository.save(modelMapper.map(userDto, Supplier.class));
        }
        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public UserDTO update(final UserDTO userDto) {
        User u = userRepository.findById(userDto.getId()).orElse(null);
        if (u == null) {
            return null;
        }

        u.setUsername(userDto.getUsername());
        u.setPassword(userDto.getPassword());
        u.setFirstName(userDto.getFirstName());
        u.setLastName(userDto.getLastName());
        u.setEmail(userDto.getEmail());
        u.setAddress(userDto.getAddress());
        u.setNumber(userDto.getNumber());

        User u1 = userRepository.save(u);

        return modelMapper.map(u1, UserDTO.class);
    }

    @Override
    public List<User> getAllBuySup() {
        List<User> all = userRepository.findAll();
        List<User> res = new ArrayList<>();
        for (User u : all) {
            if (!u.getRole().equals(Role.ADMIN) && !u.isDeleted()) {
                res.add(u);
            }
        }
        return res;
    }

    @Override
    public void changeRole(Integer id) {
        User u = userRepository.findById(id).orElse(null);
        if (u != null) {
            if (u.getRole().equals(Role.SUPPLIER)) {
                Supplier s = (Supplier) u;

                for (Cart c : s.getCarts()) {
                    for (BoughtItem bi : c.getBought()) {
                        boughtItemRepository.deleteBI(bi.getId());
                    }
                    cartRepository.deleteC(c.getId());
                }

                s.setDeleted(true);

                Buyer b = new Buyer();
                b.setFirstName(s.getFirstName());
                b.setLastName(s.getLastName());
                b.setUsername(s.getUsername());
                b.setPassword(s.getPassword());
                b.setEmail(s.getEmail());
                b.setAddress(s.getAddress());
                b.setNumber(s.getNumber());
                b.setDeleted(false);
                b.setRole(Role.BUYER);
                userRepository.save(s);
                userRepository.save(b);

            } else if (u.getRole().equals(Role.BUYER)) {
                Buyer b1 = (Buyer) u;
                for (Cart c : b1.getCarts()) {
                    for (BoughtItem bi : c.getBought()) {
                        boughtItemRepository.deleteBI(bi.getId());
                    }
                    cartRepository.deleteC(c.getId());
                }
                for (FavoriteItem fi : b1.getFavoriteItems()) {
                    favoriteItemRepository.deleteFI(fi.getId());
                }

                for (ItemInCart iic : b1.getItemsInCart()) {
                    itemInCartRepository.deleteIIC(iic.getId());
                }
                b1.setDeleted(true);

                Supplier s1 = new Supplier();
                s1.setFirstName(b1.getFirstName());
                s1.setLastName(b1.getLastName());
                s1.setUsername(b1.getUsername());
                s1.setPassword(b1.getPassword());
                s1.setEmail(b1.getEmail());
                s1.setAddress(b1.getAddress());
                s1.setNumber(b1.getNumber());
                s1.setDeleted(false);
                s1.setRole(Role.SUPPLIER);
                userRepository.save(b1);
                userRepository.save(s1);

            }
        }

    }

    @Override
    public void deleteSup(Integer id) {
        User u = userRepository.findById(id).orElse(null);
        if (u != null) {
            if (u.getRole().equals(Role.SUPPLIER)) {
                Supplier s = (Supplier) u;

                for (Cart c : s.getCarts()) {
                    for (BoughtItem bi : c.getBought()) {
                        boughtItemRepository.deleteBI(bi.getId());
                    }
                    cartRepository.deleteC(c.getId());
                }

                s.setDeleted(true);
                userRepository.save(s);
            }
        }
    }

    @Override
    public List<User> getAllSup() {
        List<User> all = userRepository.findAll();
        List<User> res = new ArrayList<>();
        for (User u : all) {
            if (u.getRole().equals(Role.SUPPLIER) && !u.isDeleted()) {
                res.add(u);
            }
        }
        return res;
    }
}
