package com.ftn.serviceImpl;

import com.ftn.dto.ItemDTO;
import com.ftn.model.*;
import com.ftn.repository.*;
import com.ftn.service.ItemService;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private FavoriteItemRepository favoriteItemRepository;

    @Autowired
    private ItemInCartRepository itemInCartRepository;

    @Autowired
    private BoughtItemRepository boughtItemRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ItemDTO> getAll() {
        List<Item> items = itemRepository.findAll();
        List<ItemDTO> ret = new ArrayList<>();
        for (Item i : items) {
            if (i.getQuantity() > 0) {
                ret.add(modelMapper.map(i, ItemDTO.class));
            }
        }
        return ret;
    }

    @Override
    public List<ItemDTO> getAllFavoritesById(Integer id) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null || user.getRole() != Role.BUYER) {
            return null;
        }
        Buyer b = (Buyer) user;

        List<ItemDTO> items = new ArrayList<>();
        for (FavoriteItem fi : b.getFavoriteItems()) {
            items.add(modelMapper.map(itemRepository.getOne(fi.getItem_id()), ItemDTO.class));
        }
        return items;
    }


    @Override
    public List<ItemDTO> getAllInCartById(Integer id) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null || user.getRole() != Role.BUYER) {
            return null;
        }
        Buyer b = (Buyer) user;

        List<ItemDTO> items = new ArrayList<>();
        for (ItemInCart ic : b.getItemsInCart()) {
            ItemDTO m = modelMapper.map(itemRepository.getOne(ic.getItem_id()), ItemDTO.class);
            m.setQuantity(ic.getQuantity());
            items.add(m);
        }
        return items;
    }


    @Override
    public List<ItemDTO> getAllPossiblesById(Integer id) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null || user.getRole() != Role.BUYER) {
            return null;
        }
        Buyer b = (Buyer) user;

        List<Item> all = itemRepository.findAll();

        List<ItemDTO> possibles = new ArrayList<>();
        for (Item i : all) {
            boolean postoji = false;
            for (FavoriteItem fi : b.getFavoriteItems()) {
                if (i.getId().equals(fi.getItem_id())) {
                    postoji = true;
                }
            }
            if (!postoji) {
                possibles.add(modelMapper.map(i, ItemDTO.class));
            }
        }


        return possibles;
    }

    @Override
    public void deleteFavoriteItem(FavoriteItem fi) {

        FavoriteItem f = favoriteItemRepository.findByBuyer_idAndItem_id(fi.getBuyer_id(), fi.getItem_id());
        favoriteItemRepository.delete(f);

    }

    @Override
    public FavoriteItem saveFavoriteItem(FavoriteItem fi) {
        return favoriteItemRepository.save(fi);
    }

    @Override
    public void deleteItemInCart(ItemInCart ic) {
        ItemInCart i = itemInCartRepository.findByBuyer_idAndItem_id(ic.getBuyer_id(), ic.getItem_id());
        itemInCartRepository.delete(i);
    }

    @Override
    public void addItemInCart(ItemInCart ic) {

        ItemInCart i = itemInCartRepository.findByBuyer_idAndItem_id(ic.getBuyer_id(), ic.getItem_id());
        if (i == null) {
            itemInCartRepository.save(ic);
        } else {
            i.setQuantity(i.getQuantity() + ic.getQuantity());
            itemInCartRepository.save(i);
        }
    }

    @Override
    public void updateItemInCart(ItemInCart ic) {

        ItemInCart i = itemInCartRepository.findByBuyer_idAndItem_id(ic.getBuyer_id(), ic.getItem_id());
        i.setQuantity(ic.getQuantity());
        itemInCartRepository.save(i);
    }

    @Override
    public List<ItemDTO> getAllDiscount() {
        List<Item> items = itemRepository.findAll();
        List<ItemDTO> ret = new ArrayList<>();
        for (Item i : items) {
            if (i.getQuantity() > 0 && i.isDiscount()) {
                ret.add(modelMapper.map(i, ItemDTO.class));
            }
        }
        return ret;
    }

    @Override
    public void updateDisc(Integer id) {
        Item item = itemRepository.findById(id).orElse(null);
        item.setDiscount(!item.isDiscount());
        itemRepository.save(item);
    }

    @Override
    public ItemDTO saveItem(ItemDTO itemDTO) {
        itemDTO.setDiscount(false);
        Item i = itemRepository.save(modelMapper.map(itemDTO, Item.class));
        return modelMapper.map(i, ItemDTO.class);
    }

    @Override
    public ItemDTO updateItem(ItemDTO itemDTO) {
        Item i = itemRepository.findById(itemDTO.getId()).orElse(null);
        if (i == null) {
            return null;
        }
        i.setQuantity(itemDTO.getQuantity());
        i.setPrice(itemDTO.getPrice());
        i.setName(itemDTO.getName());
        i.setCategory(itemDTO.getCategory());
        i.setDescription(itemDTO.getDescription());
        Item save = itemRepository.save(i);
        return modelMapper.map(save, ItemDTO.class);
    }

    @Override
    public void deleteItem(Integer id) {

        boughtItemRepository.deleteByIteem(id);
        favoriteItemRepository.deleteByIteem(id);
        itemInCartRepository.deleteByIteem(id);
        itemRepository.deleteItem(id);

    }
}
