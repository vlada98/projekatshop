package com.ftn.repository;

import com.ftn.model.Cart;
import com.ftn.model.StatusOfDelivery;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CartRepository extends JpaRepository<Cart, Integer> {
    @Query("Select c from Cart c where c.buyer_id =:buyerId")
    List<Cart> findByBuyer_id(@Param("buyerId") Integer buyerId);

    @Transactional
    @Modifying
    // sql jezik         update cart set status = 'nesto' where id =1
    @Query("Update Cart c set c.status =:newStatus where c.id =:id")
    void changeStatusOfCart(@Param("id") Integer id, @Param("newStatus") StatusOfDelivery newStatus);


    @Transactional
    @Modifying
    @Query("DELETE from Cart c where c.id =:id")
    void deleteC(@Param("id") Integer id);
}