package com.ftn.repository;

import com.ftn.model.BoughtItem;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BoughtItemRepository extends JpaRepository<BoughtItem, Integer> {

    @Transactional
    @Modifying
    @Query("DELETE from BoughtItem bi where bi.id =:id")
    void deleteBI(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("DELETE from BoughtItem bi where bi.item_id =:id")
    void deleteByIteem(@Param("id") Integer id);
}
