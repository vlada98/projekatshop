package com.ftn.repository;

import com.ftn.model.Item;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ItemRepository extends JpaRepository<Item, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE from Item i where i.id =:id")
    void deleteItem(@Param("id") Integer id);
}
