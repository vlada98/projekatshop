package com.ftn.repository;

import com.ftn.model.ItemInCart;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ItemInCartRepository extends JpaRepository<ItemInCart, Integer> {

    @Query("Select fi from ItemInCart fi where fi.buyer_id =:buyerId and fi.item_id =:itemId ")
    ItemInCart findByBuyer_idAndItem_id(@Param("buyerId") Integer buyerId, @Param("itemId") Integer item_id);

    @Transactional
    @Modifying
    @Query("DELETE from ItemInCart fi where fi.id =:id")
    void deleteIIC(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("DELETE from ItemInCart fi where fi.item_id =:id")
    void deleteByIteem(@Param("id") Integer id);
}
