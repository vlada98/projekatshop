package com.ftn.repository;

import com.ftn.model.FavoriteItem;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FavoriteItemRepository extends JpaRepository<FavoriteItem, Integer> {
    @Query("Select fi from FavoriteItem fi where fi.buyer_id =:buyerId and fi.item_id =:itemId ")
    FavoriteItem findByBuyer_idAndItem_id(@Param("buyerId") Integer buyerId, @Param("itemId") Integer itemId);


    @Transactional
    @Modifying
    @Query("DELETE from FavoriteItem fi where fi.id =:id")
    void deleteFI(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("DELETE from FavoriteItem fi where fi.item_id =:id")
    void deleteByIteem(@Param("id") Integer id);
}
