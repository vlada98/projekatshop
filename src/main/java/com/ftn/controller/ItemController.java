package com.ftn.controller;

import com.ftn.dto.ItemDTO;
import com.ftn.model.FavoriteItem;
import com.ftn.model.ItemInCart;
import com.ftn.service.ItemService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ItemController {

    @Autowired
    private ItemService itemService;

    @PostMapping("saveItem")
    public ItemDTO save(@RequestBody ItemDTO itemDTO) {
        return itemService.saveItem(itemDTO);
    }

    @PutMapping("updateItem")
    public ItemDTO update(@RequestBody ItemDTO itemDTO) {
        return itemService.updateItem(itemDTO);
    }

    @DeleteMapping("deleteItem/{id}")
    public void deleteItem(@PathVariable("id") Integer id) {
        itemService.deleteItem( id);
    }

    @GetMapping("items")
    public List<ItemDTO> getAll() {
        return itemService.getAll();
    }

    @GetMapping("itemsDiscount")
    public List<ItemDTO> getAllDiscount() {
        return itemService.getAllDiscount();
    }

    @GetMapping("itemsFav/{id}")
    public List<ItemDTO> getAllItemsUser(@PathVariable("id") Integer id) {
        return itemService.getAllFavoritesById(id);
    }


    @GetMapping("itemsCart/{id}")
    public List<ItemDTO> getAllCartItemsUser(@PathVariable("id") Integer id) {
        return itemService.getAllInCartById(id);
    }


    @GetMapping("itemsPossibles/{id}")
    public List<ItemDTO> getAllPossiblesUser(@PathVariable("id") Integer id) {
        System.out.println("POZVAO SA " + id);
        return itemService.getAllPossiblesById(id);
    }

    @DeleteMapping("deleteFav")
    public void deleteFav(@RequestBody FavoriteItem fi) {
        itemService.deleteFavoriteItem(fi);
    }

    @DeleteMapping("deleteItemCart")
    public void deleteFav(@RequestBody ItemInCart ic) {
        itemService.deleteItemInCart(ic);
    }

    @PostMapping("saveFav")
    public FavoriteItem saveFav(@RequestBody FavoriteItem fi) {
        return itemService.saveFavoriteItem(fi);
    }


    @PostMapping("saveItemCart")
    public void saveItemCart(@RequestBody ItemInCart fi) {
        itemService.addItemInCart(fi);
    }


    @PutMapping("updateItemCart")
    public void updateItemCart(@RequestBody ItemInCart fi) {
        itemService.updateItemInCart(fi);
    }

    @PutMapping("updateDisc/{id}")
    public void updateDisc(@PathVariable("id") Integer id) {
        itemService.updateDisc(id);
    }
}
