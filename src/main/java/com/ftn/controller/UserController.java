package com.ftn.controller;

import com.ftn.dto.UserDTO;
import com.ftn.dto.UserLoginDTO;
import com.ftn.model.User;
import com.ftn.service.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("login")
	public UserDTO login(@RequestBody UserLoginDTO user) {
		System.out.println("DOBIO: " + user.getUsername() + " " + user.getPassword());
		return userService.login(user);
	}

	@PostMapping("registration")
	public UserDTO registration(@RequestBody UserDTO userDto) {
		return userService.save(userDto);
	}

	@PutMapping("update")
	public UserDTO update(@RequestBody UserDTO userDto) {

		return userService.update(userDto);
	}

	@GetMapping("getAllBuySup")
	public List<User> get()
	{
		return userService.getAllBuySup();
	}

	@GetMapping("getAllSup")
	public List<User> getSup()
	{
		return userService.getAllSup();
	}

	@PutMapping("changeRole/{id}")
	void changeRole(@PathVariable("id") Integer id)
	{
		userService.changeRole(id);
	}

	@DeleteMapping("deleteSup/{id}")
	void deleteSup(@PathVariable("id") Integer id)
	{
		userService.deleteSup(id);
	}

}
