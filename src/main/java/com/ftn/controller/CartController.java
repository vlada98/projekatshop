package com.ftn.controller;

import com.ftn.dto.CartDTO;
import com.ftn.dto.CartSupplierDTO;
import com.ftn.dto.DateDTO;
import com.ftn.dto.StatusDTO;
import com.ftn.model.Cart;
import com.ftn.service.CartService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartController {
    @Autowired
    private CartService cartService;

    @PostMapping(value = "updateCart/{id}")
    void update(@PathVariable("id") Integer id, @RequestBody StatusDTO status) {
        System.out.println("DOBIO SAM: " + status.getData());
        cartService.changeStatusOfCart(id, status.getData());
    }

    @PostMapping(value = "updateCartInProgress")
    void updateCartSetSup(@RequestBody CartSupplierDTO cartSupplierDTO) {
        cartService.updateCartSetSup(cartSupplierDTO);
    }

    @PostMapping(value = "saveCart")
    void save(@RequestBody Cart cart) {
        cartService.saveCart(cart);
    }

    @GetMapping(value = "getAllCartsWithoutSup")
    List<Cart> getAllWithoutSup() {
        return cartService.getAllWithoutSup();
    }

    @GetMapping(value = "getAllCartsByIdSup/{id}")
    List<Cart> getAllWithoutSup(@PathVariable("id") Integer id) {
        return cartService.getAllByIdSup(id);
    }




    @PostMapping(value = "dayReport")
    List<CartDTO> dayReport(@RequestBody DateDTO dateDTO) {
        return cartService.getDayReport(dateDTO);
    }



    @PostMapping(value = "weekReport")
    List<CartDTO> weekReport(@RequestBody DateDTO dateDTO) {
        return cartService.getWeekReport(dateDTO);
    }



    @PostMapping(value = "monthReport")
    List<CartDTO> monthReport(@RequestBody DateDTO dateDTO) {
        return cartService.getMonthReport(dateDTO);
    }

}
