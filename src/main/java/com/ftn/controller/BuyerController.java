package com.ftn.controller;

import com.ftn.dto.CartDTO;
import com.ftn.service.BuyerService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BuyerController {
    @Autowired
    private BuyerService buyerService;

    @GetMapping("allCarts/{id}")
    public List<CartDTO> getAll(@PathVariable("id") Integer id) {

        return buyerService.getAllCartsByBuyerId(id);
    }

}
