package com.ftn;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ftn.model.Admin;
import com.ftn.model.BoughtItem;
import com.ftn.model.Buyer;
import com.ftn.model.Cart;
import com.ftn.model.Category;
import com.ftn.model.FavoriteItem;
import com.ftn.model.Item;
import com.ftn.model.ItemInCart;
import com.ftn.model.Role;
import com.ftn.model.StatusOfDelivery;
import com.ftn.model.Supplier;
import com.ftn.repository.BoughtItemRepository;
import com.ftn.repository.CartRepository;
import com.ftn.repository.FavoriteItemRepository;
import com.ftn.repository.ItemInCartRepository;
import com.ftn.repository.ItemRepository;
import com.ftn.repository.UserRepository;

@SpringBootApplication
public class OnlineShop {

	public static void main(String[] args) {
		SpringApplication.run(OnlineShop.class, args);
	}

	@Bean
	CommandLineRunner run(ItemInCartRepository itemInCartRepository, BoughtItemRepository boughtItemRepository, CartRepository cartRepository,
		FavoriteItemRepository favoriteItemRepository, ItemRepository itemRepository, UserRepository userRepository) {
		return args -> {

			//            Admin nn;
			//            System.out.println(nn.getFirstName());

			Admin a = new Admin("laza", "laza", "laza", "laza", Role.ADMIN, "123", "pera@gmail.com", "a", false);

			Supplier s = new Supplier("pera", "pera", "pera", "pera", Role.SUPPLIER, "123", "pera@gmail.com", "a", false);

			Supplier s1 = new Supplier("milovan", "milovan", "milovan", "milovan", Role.SUPPLIER, "123", "milovan@gmail.com", "neka", false);

			Buyer b = new Buyer("zika", "zika", "zika", "zika", Role.BUYER, "123", "pera@gmail.com", "a", false);

			Buyer b2 = new Buyer("sima", "sima", "sima", "sima", Role.BUYER, "123", "pera@gmail.com", "a", false);

			userRepository.save(a);
			userRepository.save(s1);
			Supplier Srez = userRepository.save(s);
			Buyer Bez = userRepository.save(b);
			Buyer B2rez = userRepository.save(b2);

			Item i1 = new Item("kola", "kola", 12, 43, Category.DRNIK, true);

			Item i2 = new Item("burger", "burger", 12, 72, Category.FOOD, false);

			Item i3 = new Item("pomfrit", "lepi", 12, 87, Category.FOOD, false);

			Item i4 = new Item("pepsi", "burger", 125, 52, Category.DRNIK, false);

			Item i5 = new Item("sok", "kola", 125, 16, Category.DRNIK, false);

			Item i6 = new Item("fanta", "burger", 125, 28, Category.DRNIK, false);

			Item i7 = new Item("sprite", "burger", 125, 98, Category.DRNIK, false);

			Item i8 = new Item("pereca", "burger", 12, 80, Category.FOOD, false);

			Item i9 = new Item("keks", "lepi", 12, 64, Category.FOOD, false);

			Item i10 = new Item("kupus", "ruzni", 125, 23, Category.FOOD, false);

			Item i11 = new Item("pasulj", "kola", 125, 58, Category.FOOD, false);

			Item i12 = new Item("limunada", "burger", 125, 79, Category.DRNIK, false);

			Item i1Rez = itemRepository.save(i1);
			itemRepository.save(i2);

			itemRepository.save(i3);
			itemRepository.save(i4);
			itemRepository.save(i5);
			Item i6Rez = itemRepository.save(i6);
			Item i7Rez = itemRepository.save(i7);
			Item i8Rez = itemRepository.save(i8);
			itemRepository.save(i9);
			itemRepository.save(i10);
			itemRepository.save(i11);
			itemRepository.save(i12);

			Cart c = new Cart(LocalDate.of(2019, 7, 9), StatusOfDelivery.DELIVERED, Srez.getId(), Bez.getId());

			Cart cREz = cartRepository.save(c);

			BoughtItem bi = new BoughtItem(cREz.getId(), i1Rez.getId(), 1);
			BoughtItem bi2 = new BoughtItem(cREz.getId(), i6Rez.getId(), 1);
			boughtItemRepository.save(bi);
			boughtItemRepository.save(bi2);

			Cart c2 = new Cart(LocalDate.now(), StatusOfDelivery.CANCELED, null, Bez.getId());

			Cart cREz2 = cartRepository.save(c2);

			BoughtItem bi21 = new BoughtItem(cREz2.getId(), i7Rez.getId(), 2);
			BoughtItem bi22 = new BoughtItem(cREz2.getId(), i8Rez.getId(), 3);
			boughtItemRepository.save(bi21);
			boughtItemRepository.save(bi22);

			Cart c3 = new Cart(LocalDate.of(2019, 7, 2), StatusOfDelivery.DELIVERED, null, Bez.getId());

			Cart cREz3 = cartRepository.save(c3);

			BoughtItem bi213 = new BoughtItem(cREz3.getId(), i10.getId(), 6);
			BoughtItem bi223 = new BoughtItem(cREz3.getId(), i6Rez.getId(), 3);
			boughtItemRepository.save(bi213);
			boughtItemRepository.save(bi223);

			FavoriteItem fi = new FavoriteItem(Bez.getId(), i1Rez.getId());
			FavoriteItem fi2 = new FavoriteItem(Bez.getId(), i6Rez.getId());
			FavoriteItem fi3 = new FavoriteItem(B2rez.getId(), i1Rez.getId());

			FavoriteItem fi4 = new FavoriteItem(Bez.getId(), i7Rez.getId());

			FavoriteItem fi5 = new FavoriteItem(Bez.getId(), i8Rez.getId());

			favoriteItemRepository.save(fi);
			favoriteItemRepository.save(fi2);
			favoriteItemRepository.save(fi3);
			favoriteItemRepository.save(fi4);
			favoriteItemRepository.save(fi5);

			ItemInCart iic = new ItemInCart(Bez.getId(), i1Rez.getId(), 2);

			itemInCartRepository.save(iic);

			ItemInCart iic2 = new ItemInCart(Bez.getId(), i6Rez.getId(), 1);

			itemInCartRepository.save(iic2);
		};
	}
}
