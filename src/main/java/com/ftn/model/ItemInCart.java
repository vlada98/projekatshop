package com.ftn.model;

import javax.persistence.*;

@Entity
public class ItemInCart {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column
    private Integer buyer_id;

    @Column
    private Integer item_id;

    @Column
    private int quantity;

    public ItemInCart() {
    }

    public ItemInCart(Integer buyer_id, Integer item_id, int quantity) {
        this.buyer_id = buyer_id;
        this.item_id = item_id;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(Integer buyer_id) {
        this.buyer_id = buyer_id;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
