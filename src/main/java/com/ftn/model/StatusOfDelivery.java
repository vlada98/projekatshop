package com.ftn.model;

public enum StatusOfDelivery {
    BOUGHT, IN_PROGRESS, CANCELED, DELIVERED
}
