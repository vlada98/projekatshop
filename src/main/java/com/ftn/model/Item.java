package com.ftn.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Item {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private double price;

    @Column
    private int quantity;

    @Enumerated(EnumType.STRING)
    @Column
    private Category category;

    @Column
    private boolean discount;

    // ADMIN STAVI na popust artikle

    // NOVA STRANA NA POCETKU GDE SE GLEDAJU SAMO ONI NA POPUSTU

    //

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "item_id")
    private List<FavoriteItem> favs = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "item_id")
    private List<ItemInCart> inCartList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "item_id")
    private List<BoughtItem> bought = new ArrayList<>();

    public Item() {
    }

    public Item(String name, String description, double price, int quantity, Category category, boolean discount) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
        this.discount = discount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<FavoriteItem> getFavs() {
        return favs;
    }

    public void setFavs(List<FavoriteItem> favs) {
        this.favs = favs;
    }

    public List<ItemInCart> getInCartList() {
        return inCartList;
    }

    public void setInCartList(List<ItemInCart> inCartList) {
        this.inCartList = inCartList;
    }

    public List<BoughtItem> getBought() {
        return bought;
    }

    public void setBought(List<BoughtItem> bought) {
        this.bought = bought;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", category=" + category +
                ", favs=" + favs +
                ", inCartList=" + inCartList +
                ", bought=" + bought +
                ", discount=" + discount +
                '}';
    }
}
