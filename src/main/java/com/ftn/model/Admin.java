package com.ftn.model;

import javax.persistence.Entity;

@Entity
public class Admin extends User {

    public Admin() {
    }

    public Admin(String username, String password, String firstName, String lastName, Role role, String number, String email, String adress, boolean deleted) {
        super(username, password, firstName, lastName, role, number, email, adress, deleted);
    }
}
