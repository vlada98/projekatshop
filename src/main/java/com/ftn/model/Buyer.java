package com.ftn.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Buyer extends User {

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "buyer_id")
    private List<Cart> carts = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "buyer_id")
    private List<FavoriteItem> favoriteItems = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "buyer_id")
    private List<ItemInCart> itemsInCart = new ArrayList<>();

    public Buyer() {
        super();
    }

    public Buyer(String username, String password, String firstName, String lastName, Role role, String number, String email, String adress, boolean deleted) {
        super(username, password, firstName, lastName, role, number, email, adress, deleted);
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    public List<FavoriteItem> getFavoriteItems() {
        return favoriteItems;
    }

    public void setFavoriteItems(List<FavoriteItem> favoriteItems) {
        this.favoriteItems = favoriteItems;
    }

    public List<ItemInCart> getItemsInCart() {
        return itemsInCart;
    }

    public void setItemsInCart(List<ItemInCart> itemsInCart) {
        this.itemsInCart = itemsInCart;
    }

    @Override
    public String toString() {
        return "Buyer{" +
                ", carts=" + carts +
                ", favoriteItems=" + favoriteItems +
                ", itemsInCart=" + itemsInCart +
                '}';
    }
}
