package com.ftn.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Supplier extends User {
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "supplier_id")
    private List<Cart> carts = new ArrayList<>();

    public Supplier() {
    }

    public Supplier(String username, String password, String firstName, String lastName, Role role, String number, String email, String adress, boolean deleted) {
        super(username, password, firstName, lastName, role, number, email, adress, deleted);
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    @Override
    public String toString() {
        return "Supplier{" +
                "carts=" + carts +
                '}';
    }
}
