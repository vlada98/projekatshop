package com.ftn.model;

import javax.persistence.*;

@Entity
public class BoughtItem { // ova klasa predstavlja kupljeni item u korpi
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column
    private Integer cart_id;

    @Column
    private Integer item_id;

    @Column
    private Integer totalQuantity;

    public BoughtItem() {
    }

    public BoughtItem(Integer cart_id, Integer item_id, Integer totalQuantity) {
        this.cart_id = cart_id;
        this.item_id = item_id;
        this.totalQuantity = totalQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCart_id() {
        return cart_id;
    }

    public void setCart_id(Integer cart_id) {
        this.cart_id = cart_id;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    @Override
    public String toString() {
        return "BoughtItem{" +
                "id=" + id +
                ", cart_id=" + cart_id +
                ", item_id=" + item_id +
                ", totalQuantity=" + totalQuantity +
                '}';
    }
}
