package com.ftn.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Cart {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column
    private StatusOfDelivery status;

    @Column
    private Integer supplier_id;

    @Column
    private Integer buyer_id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cart_id")
    private List<BoughtItem> bought = new ArrayList<>();

    public Cart() {
    }

    public Cart(LocalDate date, StatusOfDelivery status, Integer supplier_id, Integer buyer_id) {
        this.date = date;
        this.status = status;
        this.supplier_id = supplier_id;
        this.buyer_id = buyer_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public StatusOfDelivery getStatus() {
        return status;
    }

    public void setStatus(StatusOfDelivery status) {
        this.status = status;
    }

    public Integer getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(Integer supplier_id) {
        this.supplier_id = supplier_id;
    }

    public Integer getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(Integer buyer_id) {
        this.buyer_id = buyer_id;
    }

    public List<BoughtItem> getBought() {
        return bought;
    }

    public void setBought(List<BoughtItem> bought) {
        this.bought = bought;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", date=" + date +
                ", status=" + status +
                ", supplier_id=" + supplier_id +
                ", buyer_id=" + buyer_id +
                ", bought=" + bought +
                '}';
    }
}
