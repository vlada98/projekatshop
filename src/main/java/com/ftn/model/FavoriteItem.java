package com.ftn.model;

import javax.persistence.*;

@Entity
public class FavoriteItem { // ova klasa predstalvja omiljeni artikal od strane kupca
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @Column
    private Integer buyer_id;

    @Column
    private Integer item_id;

    public FavoriteItem() {
    }

    public FavoriteItem(Integer buyer_id, Integer item_id) {
        this.buyer_id = buyer_id;
        this.item_id = item_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(Integer buyer_id) {
        this.buyer_id = buyer_id;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    @Override
    public String toString() {
        return "FavoriteItem{" +
                "id=" + id +
                ", buyer_id=" + buyer_id +
                ", item_id=" + item_id +
                '}';
    }
}
