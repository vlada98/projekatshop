$(document).ready(function () {


    $("#login").click(function (event) {
        event.preventDefault();
        console.log("POZVAO");
        $.ajax({
            type: "post",
            dataType: 'json',
            contentType: 'application/json',
            url: "/login",


            data: JSON.stringify({username: $("#username").val(), password: $("#password").val()}),
            success: function (data) {
                localStorage.setItem("loggedUser",JSON.stringify(data));

                if (data["role"].toLowerCase() === "buyer")
                {
                    window.location.href = "/buyer.html"
                }
                else if (data["role"].toLowerCase() === "admin")
                {
                    window.location.href = "/admin.html"
                }
                else {
                    window.location.href = "/supplier.html"
                }

            },
            error: function (data) {
                $("#wrong-login").show();
                //alert("Error: " + JSON.stringify(data));
            }
        });
    });
});

