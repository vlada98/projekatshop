var table;
var dataTable = [];
var cats = [];
var indexZaEdit = -1;


function makeTable(provera) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable
            ,
            'columns': [
                {"data": "username"},
                {"data": "firstName"},
                {"data": "lastName"},
                {"data": "email"},
                {"data": "address"},
                {"data": "number"}
            ]

        });

    if (provera) return;

    $('#datatable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            console.log("prvi");

            $('#edit').prop("disabled", true);
            $('#obrisi').prop("disabled", true);

            $(this).removeClass('selected');
        }
        else {
            console.log("drugi");
            $('#edit').prop("disabled", false);
            $('#obrisi').prop("disabled", false);

            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });


    console.log("ALO");
    table.draw();

}

$(document).ready(function () {

    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });


    $('#edit').prop("disabled", true);
    $('#obrisi').prop("disabled", true);

    $('#obrisi').click(function () {
        $('#edit').prop("disabled", true);
        // table.row('.selected').remove().draw( false );
        var indexZaBrisanje = table.row('.selected')["0"];

        console.log(dataTable[indexZaBrisanje]["id"])

        $.ajax({
            type: "delete",
            url: "/deleteSup/" + dataTable[indexZaBrisanje]["id"],
            success: function (d) {
                table.destroy()
                loadData(true);
            }
        });


    });

    $('#edit').click(function () {
        $('#obrisi').prop("disabled", true);
        $("#okiniEdit").click();
        var da = table.row('.selected')["0"];
        indexZaEdit = dataTable[da]["id"];


        $("#addressU").val(dataTable[da]["address"])
        $("#emailU").val(dataTable[da]["email"])
        $("#firstNameU").val(dataTable[da]["firstName"])
        $("#lastNameU").val(dataTable[da]["lastName"])
        $("#numberU").val(dataTable[da]["number"])
        $("#passwordU").val(dataTable[da]["password"])
        $("#usernameU").val(dataTable[da]["username"])

    });

    $('#add').click(function () {
        $("#okiniAdd").click();
    });


    $('#update').click(function () {

        $.ajax({
            type: "put",
            contentType: 'application/json',
            url: "/update",
            data: makeDataU(),
            success: function (data) {
                table.destroy();
                loadData(true);
            },
        });

    });


    $('#save').click(function () {

        $.ajax({
            type: "post",
            dataType: 'json',
            contentType: 'application/json',
            url: "/registration",
            data: makeData(),
            success: function (data) {
                table.destroy();
                loadData(true);
            },
        });

    });

    loadData(false);


});

function loadData(provera) {


    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/getAllSup",
        success: function (data) {
            dataTable = [];
            for (var i = 0; i < data.length; i++) {
                dataTable.push(
                    {
                        "id": data[i]["id"],
                        "username": data[i]["username"],
                        "role": data[i]["role"],
                        "number": data[i]["number"],
                        "password": data[i]["password"],
                        "firstName": data[i]["firstName"],
                        "lastName": data[i]["lastName"],
                        "email": data[i]["email"],
                        "address": data[i]["address"]
                    }
                );
            }

            makeTable(provera);
        }
    });


}

function makeData() {
    var obj = {


        "address": $("#address").val(),
        "email": $("#email").val(),
        "firstName": $("#firstName").val(),
        "lastName": $("#lastName").val(),
        "number": $("#number").val(),
        "password": $("#password").val(),
        "role": "SUPPLIER",
        "username": $("#username").val(),
    };
    return JSON.stringify(obj);
}

function makeDataU() {
    var obj = {

        "id": indexZaEdit,
        "address": $("#addressU").val(),
        "email": $("#emailU").val(),
        "firstName": $("#firstNameU").val(),
        "lastName": $("#lastNameU").val(),
        "number": $("#numberU").val(),
        "password": $("#passwordU").val(),
        "role": "SUPPLIER",
        "username": $("#usernameU").val()

    };
    return JSON.stringify(obj);
}

