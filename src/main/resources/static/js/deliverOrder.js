var currentUser = localStorage.getItem('loggedUser');
currentUser = JSON.parse(currentUser);

var table;
var dataTable = [];


function makeTable(provera) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable
            ,
            'columns': [
                {"data": "date"},
                {"data": "status"},
                {"data" : "numOfItems"}
            ]

        });

    if (provera){
        return
    }


    $('#datatable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            console.log("prvi");

            $('#add').prop("disabled",true);

            $(this).removeClass('selected');
        }
        else {
            console.log("drugi");
            $('#add').prop("disabled",false);

            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');


        }
    } );


    table.draw();

}

$(document).ready(function () {


    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });

    $('#add').click(function () {

        var ko = table.row('.selected')["0"];
        var selekt = dataTable[ko];
        console.log("Selekt: " + selekt["id"])

        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "/updateCart/"+selekt["id"],
            data: JSON.stringify(
                {
                    "data": "DELIVERED"
                }
            ) ,
            success: function (data) {

                table.destroy();
                loadData(true);

            }

        });
    });

    $('#add').prop("disabled",true);

    loadData(false)


});

function loadData(provera)
{
    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/getAllCartsByIdSup/"+currentUser["id"],
        success: function (data) {
            dataTable = [];
            for(var i = 0 ; i < data.length;i++)
            {
                dataTable.push(
                    {
                        "id" : data[i]["id"],
                        "date": data[i]["date"],
                        "status": data[i]["status"],
                        "numOfItems" : data[i]["bought"].length
                    }
                );
            }
            makeTable(provera);
        }
    });
}
