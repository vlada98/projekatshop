var currentUser = localStorage.getItem('loggedUser');
currentUser = JSON.parse(currentUser);

var table;

var r = [];
var imena = [];

var dataTabela = [];
$(document).ready(function () {


    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });



    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "itemsFav/" + currentUser["id"],
        success: function (d) {
            dataTabela = [];
            for (var x = 0 ; x < d.length ; x++)
            {
                dataTabela.push(d[x]);
            }
            console.log("USPESNO");
            makeTable(false);
            AllPossible(false);
        }
    });





});

///////////////////////////////////////////////////


function makeTable(ponovo) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "aaData": dataTabela
            ,
            'columns': [
                {"data": "id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "quantity"},
                {"data": "category"}
            ]

        });
    makeTableJquery(ponovo);

    table.draw();

}


function makeTableJquery(ponovo) {
    if (ponovo) return;

    $('#datatable tbody').on('click', 'tr', function () {
        console.log("USAO U HOVER");
        if ($(this).hasClass('selected')) {
            console.log("PRVI");
            $('#button').prop("disabled", true);
            $(this).removeClass('selected');
        }
        else {
            console.log("drugi");
            if (dataTabela.length !== 0) {
                $('#button').prop("disabled", false);

                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }

        }
    });

    $('#button').click(function () {
        $('#button').prop("disabled", true);

        console.log(dataTabela.length + ' ' + "size of data");


        var indexZaBrisanje = table.row('.selected')["0"];
        var currentUser = localStorage.getItem('loggedUser');
        currentUser = JSON.parse(currentUser);
        console.log("ovaj index iz data brisem " + indexZaBrisanje);
        console.log("id itema za brisanje " + make(dataTabela[indexZaBrisanje]["id"]));
        console.log();
        $.ajax({
            type: "delete",
            contentType: 'application/json',
            url: "deleteFav",
            data: make(dataTabela[indexZaBrisanje]["id"]),
            success: function (d) {
                AllPossible(true);
            }
        });
        table.row('.selected').remove().draw(false);
        dataTabela.splice(indexZaBrisanje, 1);

    });

}


function AllPossible(ponovo) {
    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/itemsPossibles/" + currentUser["id"],
        success: function (q) {

            imena = [];
            r = [];
            for (var x = 0; x < q.length; x++) {
                r.push(q[x]);
                imena.push(q[x]["name"])
            }
            AllPossibleJquery(ponovo)
        }
    });
}


function AllPossibleJquery(ponovo) {


    $("#tags").autocomplete({
        source: imena
    });

    if (ponovo) return;



    $("#tags").on('change keydown', function () {
        showVAlue();
    });

    $(document).click(function (e) {
        // Check for left button
        if (e.button == 0) {
            showVAlue();
        }
    });



    $("#add-new").click(function () {

            for (var i = 0; i < r.length; i++) {
                if (r[i]["name"] === $("#tags").val()) {

                    console.log(r[i]["id"] + " item   user" + currentUser["id"])

                    $('#add-new').prop("disabled", true);
                    $("#description").val("");
                    $("#price").val("");
                    $("#quantity").val("");
                    $("#category").val("");

                    $.ajax({
                        type: "post",
                        contentType: 'application/json',
                        url: "saveFav",
                        data: JSON.stringify(
                            {
                                "item_id": r[i]["id"],
                                "buyer_id": currentUser["id"]
                            }
                        ),
                        success: function () {
                            $.ajax({
                                type: "get",
                                contentType: 'application/json',
                                url: "itemsFav/" + currentUser["id"],
                                success: function (d) {
                                    dataTabela = [];
                                    table.destroy();
                                    for (var x = 0 ; x < d.length;x++)
                                    {
                                        dataTabela.push(d[x]);
                                    }
                                    makeTable(true);
                                    AllPossible(true);
                                }
                            });

                        }
                    });


                    break;
                }

            }
        }
    )
}

function showVAlue() {
    var x = true;
    for (var i = 0; i < r.length; i++) {
        if (r[i]["name"] === $("#tags").val()) {
            $("#description").val(r[i]["description"]);
            $("#price").val(r[i]["price"]);
            $("#quantity").val(r[i]["quantity"]);
            $("#category").val(r[i]["category"]);
            x = false;
            $('#add-new').prop("disabled", false);
            break;
        }
    }
    if (x) {
        $('#add-new').prop("disabled", true);
        $("#description").val("");
        $("#price").val("");
        $("#quantity").val("");
        $("#category").val("");
    }

}

function make(idItem) {
    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    var obj = {
        "item_id": idItem,
        "buyer_id": currentUser["id"]
    };
    return JSON.stringify(obj);
}