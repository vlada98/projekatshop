var currentUser = localStorage.getItem('loggedUser');
currentUser = JSON.parse(currentUser);

var allData = [];
var forTable = [];
var table;
var selektovanID = -1;

function makeTable(ponovo) {
     table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": forTable
            ,
            'columns': [
                {"data": "date"},
                {"data": "status"},
                {"data": "supplierFirstLastName"},
                {"data" : "numOfItems"}
            ]

        });
     if (ponovo)
     {
         return;
     }

    $('#datatable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            console.log("prvi");


            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            console.log("drugi");
            $('.table tbody').empty();

            var ko = table.row('.selected')["0"];
            var zaPrikaz = allData[ko];

            selektovanID = zaPrikaz["id"];

            if(zaPrikaz["status"] === "BOUGHT")
            {
                $("#cancel").css("display", "block");
            }
            else {
                $("#cancel").css("display", "none");

            }


            for (var x = 0 ; x< zaPrikaz["items"].length;x++)
            {
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td>'+ zaPrikaz["items"][x]["name"]+ '</td>';
                cols += '<td>'+ zaPrikaz["items"][x]["category"]+ '</td>';
                cols += '<td>'+ zaPrikaz["items"][x]["description"]+ '</td>';
                cols += '<td>'+ zaPrikaz["items"][x]["price"]+ '</td>';
                cols += '<td>'+ zaPrikaz["quantities"][x]+ '</td>';

                newRow.append(cols);
                $(".table").append(newRow);
            }




            $('#add').click();


        }
    } );
}

$(document).ready(function () {



    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });

    $("#cancel").click(function (event) {

        //


        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "/updateCart/" + selektovanID,
            data: JSON.stringify(
                {
                    "data" :"CANCELED"
                }
            ) ,
            success: function (data) {

                table.destroy();
                loadData(true);

            }

        });



    });


loadData(false);


});

function loadData(ponovo)
{
    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/allCarts/" + currentUser["id"],
        success: function (data) {
            console.log(data);
            allData = [];
            forTable = [];
            for(var i = 0 ; i < data.length;i++)
            {
                allData.push(data[i]);
                forTable.push(
                    {
                        "date": data[i]["date"],
                        "status": data[i]["status"],
                        "supplierFirstLastName": data[i]["supplierFirstLastName"],
                        "numOfItems" : data[i]["items"].length
                    }
                );

            }
            makeTable(ponovo);
        }
    });
}



