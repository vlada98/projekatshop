$(document).ready(function () {




    $("#save").click(function (event) {
        event.preventDefault();
        console.log("UPDEJTUJEM");
        $.ajax({
            type: "put",
            dataType: 'json',
            contentType: 'application/json',
            url: "/update",                     //Da li ovim dobijam samo da u bazi promeni podatke ? Da li se neki podaci dobijeni tu iz kontrolera negde cuvaju/koriste ?
            data: makeObj(),                    // ova funkcija ce da updejtuje podatke korisnika snimi u bazu i ono sto dobijem kao odgovor ce se ponovo setovati u localStorage sto mozes da vidis u success funkciji
            success: function (data) {
                localStorage.setItem("loggedUser", JSON.stringify(data));
                $("#home-tab").click();
                window.location.href = "/buyer.html"
            },
            error: function (data) {
                //alert("Error: " + JSON.stringify(data));
            }
        });
    });


    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });


    var currentUser = localStorage.getItem('loggedUser');       //Da li localStorage.getItem('loggedUser'); vraca JSON u string formatu ? DA
    currentUser = JSON.parse(currentUser);
    $("#main-name").text(currentUser["firstName"] + " " + currentUser["lastName"]);     //Jel ovo neka mapa sa kljucevima firstName, lastName i tako redom ? DA
    $("#main-role").text(currentUser["role"]);

    $("#firstName").text(currentUser["firstName"]);
    $("#lastName").text(currentUser["lastName"]);
    $("#email").text(currentUser["email"]);
    $("#number").text(currentUser["number"]);
    $("#address").text(currentUser["address"]);
    $("#username").text(currentUser["username"]);


});

function editData() {                 // funkcija se poziva kada neko stisne na tab edit details ( pretrazi ovaj naziv funkcije u htmlu kupca)                       //Gde se poziva ova funkcija, vidim samo poziv funkcije makeObj() ? Gde u kodu dolazi do preuzimanja novih podataka i izmene starih ?
    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    $("#usernameEdit").val(currentUser["username"]);
    $("#firstNameEdit").val(currentUser["firstName"]);
    $("#lastNameEdit").val(currentUser["lastName"]);
    $("#emailEdit").val(currentUser["email"]);
    $("#numberEdit").val(currentUser["number"]);
    $("#addressEdit").val(currentUser["address"]);
    $("#passwordEdit").val(currentUser["username"]);
}


function makeObj() {
    var currentUser = localStorage.getItem('loggedUser');       //U kom obliku su ovi podaci koje preuzmem sa localStorage ? Ako localStroage.getItem vraca JSON string, zasto se onda parsira,
    currentUser = JSON.parse(currentUser);                       // jel da bih mogao da preuzmem id preko currentUser["id"] ?
    var obj = {
        "id" : currentUser["id"],
        "username": $("#usernameEdit").val(),
        "password": $("#passwordEdit").val(),
        "firstName": $("#firstNameEdit").val(),
        "lastName": $("#lastNameEdit").val(),
        "role": currentUser["role"],
        "number": $("#numberEdit").val(),
        "email": $("#emailEdit").val(),
        "address": $("#addressEdit").val()
    };
    console.log("SALJEM : " + JSON.stringify(obj));
    return JSON.stringify(obj);
}