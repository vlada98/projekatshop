$(document).ready(function () {


    $("#login").click(function (event) {
        event.preventDefault();
        console.log("POZVAO");
        $.ajax({
            type: "post",
            dataType: 'json',
            contentType: 'application/json',
            url: "/login",          //da li mi vrati nesto ovde preko kontrolera ili je povratna vrednost ovo JSON.stringify... ?
                                    // json stringify sluzi samo da prebaci objekat u string . sta ti vrati login mozes da vidis u kontroleru za login
                                    // data promenljiva u funkciji success ce biti ono sto vraca kontroler
            data: JSON.stringify({username: $("#username").val(), password: $("#password").val()}),
            success: function (data) {
                localStorage.setItem("loggedUser",JSON.stringify(data));

                if (data["role"].toLowerCase() === "buyer")
                {
                    window.location.href = "/buyer.html"
                }
                else if (data["role"].toLowerCase() === "admin")
                {
                    window.location.href = "/admin.html"
                }
                else {
                    window.location.href = "/supplier.html"
                }

            },
            error: function (data) {
                $("#wrong-login").show();
                //alert("Error: " + JSON.stringify(data));
            }
        });
    });
});

