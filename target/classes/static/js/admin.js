$(document).ready(function () {



    $("#save").click(function (event) {
        event.preventDefault();
        console.log("UPDEJTUJEM");
        $.ajax({
            type: "put",
            dataType: 'json',
            contentType: 'application/json',
            url: "/update",
            data: makeObj(),
            success: function (data) {
                localStorage.setItem("loggedUser", JSON.stringify(data));
                $("#home-tab").click();
                window.location.href = "/admin.html"
            },
            error: function (data) {
                //alert("Error: " + JSON.stringify(data));
            }
        });
    });


    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });


    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    $("#main-name").text(currentUser["firstName"] + " " + currentUser["lastName"]);
    $("#main-role").text(currentUser["role"]);

    $("#firstName").text(currentUser["firstName"]);
    $("#lastName").text(currentUser["lastName"]);
    $("#email").text(currentUser["email"]);
    $("#number").text(currentUser["number"]);
    $("#address").text(currentUser["address"]);
    $("#username").text(currentUser["username"]);


});

function editData() {
    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    $("#usernameEdit").val(currentUser["username"]);
    $("#firstNameEdit").val(currentUser["firstName"]);
    $("#lastNameEdit").val(currentUser["lastName"]);
    $("#emailEdit").val(currentUser["email"]);
    $("#numberEdit").val(currentUser["number"]);
    $("#addressEdit").val(currentUser["address"]);
    $("#passwordEdit").val(currentUser["username"]);
}


function makeObj() {
    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    var obj = {
        "id": currentUser["id"],
        "username": $("#usernameEdit").val(),
        "password": $("#passwordEdit").val(),
        "firstName": $("#firstNameEdit").val(),
        "lastName": $("#lastNameEdit").val(),
        "role": currentUser["role"],
        "number": $("#numberEdit").val(),
        "email": $("#emailEdit").val(),
        "address": $("#addressEdit").val()
    };
    console.log("SALJEM : " + JSON.stringify(obj));
    return JSON.stringify(obj);
}