$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {

        var cat = $("#cat").val();
        console.log(data);
        // todo
        if (cat !== "ALL" && data[5] !== cat) {

            return false;

        }

        var name = $('#name').val();
        var description = $('#description').val();
        var price = $('#price').val();

        if (!name && !description && !price) {
            console.log("PRAZNO");
            return true;
        }
        var currentName = data[1];
        var currentDescription = data[2];
        var currentPrice = data[3];

        if (name) {
            if (name.toLowerCase() !== currentName.toLowerCase()) {
                return false;
            }
        }
        if (description) {
            if (description.toLowerCase() !== currentDescription.toLowerCase()) {
                return false;
            }
        }
        if (price) {
            if (price != currentPrice) {
                return false;
            }
        }
        return true;


        return true;
        // var min = parseInt( $('#min').val(), 10 );
        // var max = parseInt( $('#max').val(), 10 );
        // var age = parseFloat( data[3] ) || 0; // use data for the age column
        //
        // if ( ( isNaN( min ) && isNaN( max ) ) ||
        //     ( isNaN( min ) && age <= max ) ||
        //     ( min <= age   && isNaN( max ) ) ||
        //     ( min <= age   && age <= max ) )
        // {
        //     return true;
        // }
        // return false;
    }
);


function makeTable(data) {
    var table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": data
            ,
            'columns': [
                {"data": "id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "quantity"},
                {"data": "category"}
            ]

        });


    $('#name, #description, #price').keyup(function () {
        console.log("Crtam");
        table.draw();
    });

    $(' #cat').change(function () {
        console.log("Crtam");
        table.draw();
    });

}

$(document).ready(function () {

    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });

    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/items",
        success: function (data) {
            var cats = ["ALL"];
            for (var x = 0; x < data.length; x++) {
                if (!cats.includes(data[x]["category"])) {
                    cats.push(data[x]["category"]);
                }

            }
            $.each(cats, function (i, item) {
                $('#cat').append($('<option>', {
                    value: item,
                    text: item
                }));
            });


            makeTable(data);
        }
    });


});


