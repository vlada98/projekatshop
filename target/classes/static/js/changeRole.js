var currentUser = localStorage.getItem('loggedUser');
currentUser = JSON.parse(currentUser);

var table;
var dataTable = [];


function makeTable(provera) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable
            ,
            'columns': [
                {"data": "username"},
                {"data": "firstName"},
                {"data" : "lastName"},
                {"data": "role"},
                {"data": "number"},
                {"data" : "email"},
                {"data" : "address"}
            ]

        });

    if (provera){
        return
    }


    $('#datatable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            console.log("prvi");

            $('#add').prop("disabled",true);

            $(this).removeClass('selected');
        }
        else {
            console.log("drugi");
            $('#add').prop("disabled",false);

            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');


        }
    } );


    table.draw();

}

$(document).ready(function () {


    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });

    $('#add').click(function () {

        var ko = table.row('.selected')["0"];
        var selekt = dataTable[ko];
        console.log("Selekt: " + selekt["id"])

        $.ajax({
            type: "put",
            contentType: 'application/json',
            url: "/changeRole/"+selekt["id"],

            success: function (data) {

                table.destroy();
                loadData(true);

            }

        });
    });

    $('#add').prop("disabled",true);

    loadData(false)


});

function loadData(provera)
{
    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/getAllBuySup",
        success: function (data) {
            dataTable = [];
            for(var i = 0 ; i < data.length;i++)
            {
                console.log(data[i])

                dataTable.push(
                    {

                        "id" : data[i]["id"],
                        "username": data[i]["username"],
                        "password": data[i]["password"],
                        "firstName": data[i]["firstName"],
                        "lastName": data[i]["lastName"],
                        "role": data[i]["role"],
                        "number": data[i]["number"],
                        "email": data[i]["email"],
                        "address": data[i]["address"]
                    }
                );
            }
            makeTable(provera);
        }
    });
}
