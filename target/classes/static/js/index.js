$.fn.dataTable.ext.search.push( // kada se pozove crtanje tabele
    function (settings, data, dataIndex) {

        var cat = $("#cat").val(); // uzmi element sa id-em cat i njegovu vrednostu ubaci u moju varijablu cat
        console.log(data);

        if (cat !== "ALL" && data[5] !== cat) {

            return false;

        }

        var name = $('#name').val();
        var description = $('#description').val();
        var price = $('#price').val();

        if (!name && !description && !price) {
            console.log("PRAZNO");
            return true;
        }
        var currentName = data[1];
        var currentDescription = data[2];
        var currentPrice = data[3];

        if (name) {
            if (name.toLowerCase() !== currentName.toLowerCase()) {
                return false;
            }
        }
        if (description) {
            if (description.toLowerCase() !== currentDescription.toLowerCase()) {
                return false;
            }
        }
        if (price) {
            if (price != currentPrice) { // "5" ==  5 true // "5" === 5 false
                return false;
            }
        }
        return true;


    }
);


function makeTable(data) {
    var table = $('#datatable').DataTable(     //sta tacno znaci kad napisem var table = $('#datatable').DataTable ?
        {                                       // napravi se promenljiva koja se zove table i ima vrednsot tabele iz jqeruy plagina
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": data
            ,
            'columns': [
                {"data": "id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "quantity"},
                {"data": "category"}
            ]

        });


    $('#name, #description, #price').keyup(function () { // kad god neko stisne nesto na tastaturi unutar ta 3 elm
        console.log("Crtam");
        table.draw();
    });

    $('#cat').change(function () {
        console.log("Crtam");
        table.draw();
    });

}
// $(document).ready odavde krece svaki js
$(document).ready(function () {



    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/items",
        success: function (data) {
            var cats = ["ALL"];
            for (var x = 0; x < data.length; x++) {
                if (!cats.includes(data[x]["category"])) {
                    cats.push(data[x]["category"]);
                }

            }
            $.each(cats, function (i, item) {     //koji parametar je i, odakle dolazi ?  Ovde se dodaju opcije za food,drink,all ?
                $('#cat').append($('<option>', {  // each funkcija iz jqueryja prolazi kroz listu i "i" je index elementa koji nigde ni korisitm
                    value: item,                // ovde setujem kategorije food drink
                    text: item
                }));
            });


            makeTable(data);
        }
    });


});


