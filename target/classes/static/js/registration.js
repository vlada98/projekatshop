function makeData() {
    var obj = {
        "username": $("#username").val(),
        "password": $("#password").val(),
        "firstName": $("#firstname").val(),
        "lastName": $("#lastname").val(),
        "role": $("#role").val(),
        "number": $("#number").val(),
        "email": $("#email").val(),
        "address": $("#address").val()
    };
    return JSON.stringify(obj);
}

$(document).ready(function () {


    $("#registration").click(function (event) {
        event.preventDefault();


       if (!$("#username").val() ||
       !$("#password").val() ||
       !$("#firstname").val() ||
       !$("#lastname").val() ||
       !$("#role").val() ||
       !$("#number").val() ||
       !$("#email").val() ||
       !$("#address").val() )
    {
        return;
    }


        $.ajax({
            type: "post",
            dataType: 'json',
            contentType: 'application/json',
            url: "/registration",
            data: makeData(),
            success: function (data) {
                console.log("here");
                window.location.href = "/login.html"
            },
            error: function (data) {
                alert("Error: " + JSON.stringify(data));
            }
        });
    });

});
