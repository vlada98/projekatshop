var currentUser = localStorage.getItem('loggedUser');
currentUser = JSON.parse(currentUser);

var table;
var tableItems = [];
var cartItems = [];

function makeTable(provera) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": tableItems
            ,
            'columns': [
                {"data": "id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "quantity"},
                {"data": "category"}
            ]

        });

    console.log("ALO");
    table.draw();
    if (provera) return; // ako je provera true izadji necu da setujem ovo ispod

    $('#datatable tbody').on('click', 'tr', function () {   //Mozes da mi iskomentarises ili objasnis sta ovde tacno radimo, kad udjemo i sta znaci kada je provera == false ?
        if ($(this).hasClass('selected')) {                  // ovde setujem da kada neko stisne na red u tabeli da se desi magija selekta/deselekta. Provera == false znaci da prvi put pozivam funkciju i da zelim sve da uradi, ako je provera == true izadji jer necu opet da setujem ovo dole on
            console.log("prvi");                            //

            $('#add').prop("disabled", true);                          //Zasto da disejbluje ako je selektovan red u tabeli ? // U IF SE UDJE AKO JE VEC BIO SELEKTOVAN ZNACI DUGME DISEJBULJEM JER AKO SMO USLI U IF ZNACI DA DUGME VISE NIJE SELEKTOVANO
            // brisemo pozadinu oko selektovanog reda i dugme disejblujemo
            $(this).removeClass('selected');                //
        }
        else {                                              //
            console.log("drugi");
            $('#add').prop("disabled", false);              //
                                                                            // moze i bez ove linije iskuliraj je
           // table.$('tr.selected').removeClass('selected');             // ???????????????????????????????????
            $(this).addClass('selected');                          //
        }
    });


    $('#checkout').click(function () {
        console.log("KUPIO");

        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "/saveCart",
            data: d(),
            success: function (data) {
                loadCart();
                loadData(true)

            }
        });


    })

    $('#add').click(function () {
        var index = table.row('.selected')["0"];

        console.log("SALJEM ID OVAJ: " + tableItems[index]["id"]);
        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "saveItemCart",
            data: JSON.stringify(
                {
                    "quantity": 1,
                    "item_id": tableItems[index]["id"],
                    "buyer_id": currentUser["id"]
                }
            ),
            success: function () {
                loadCart();
            }
        });
    });


}
/*
   * {
"bought": [
{
 "item_id": 1,
 "totalQuantity": 2
},
{
 "item_id": 6,
 "totalQuantity": 1
}
],
"buyer_id": 3,
"date": "2019-07-02T14:52:16.407Z"
}
   * */
function d()
{
    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    var obj = {
        "buyer_id": currentUser["id"],
        "bought" : []
    };
    for (var x = 0 ; x < cartItems.length; x++)             //Gde smo popunili cartItems, kako ovo zna koliko cartItems ima elemenata ???
    {                                                       // loadCart funkcija puni cartsItems. BACEND U TOJ FUNKCIJI VRATI KOLIKO IMA ITEMA
        obj["bought"].push(
            {
                "item_id": cartItems[x]["id"],
                "totalQuantity": cartItems[x]["quantity"]
            }
        )
    }
    return JSON.stringify(obj);
}

function make(idItem) {
    var currentUser = localStorage.getItem('loggedUser');
    currentUser = JSON.parse(currentUser);
    var obj = {
        "item_id": cartItems[idItem]["id"],
        "buyer_id": currentUser["id"]
    };
    return JSON.stringify(obj);
}

function brisi(index) {
    $.ajax({
        type: "delete",
        contentType: 'application/json',
        url: "/deleteItemCart",
        data: make(index),
        success: function (d) {
            loadCart();
        }
    });


}

function novaVrednost(index) {
    console.log("Nova vrednost : " + $("#broj" + index).val())

    for (var x = 0; x < cartItems.length; x++) {
        if (x === index) {
            cartItems[x]["quantity"] = $("#broj" + index).val();
            //$.ajax({});
            $.ajax({
                type: "put",
                contentType: 'application/json',
                url: "updateItemCart",
                data: JSON.stringify(
                    {
                        "item_id": cartItems[x]["id"],
                        "buyer_id": currentUser["id"],
                        "quantity": $("#broj" + index).val()
                    }
                ),
                success: function () {
                    loadCart()

                }
            });
            break;
        }
    }

}

$(document).ready(function () {

    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });

    $(".cart").click(function (event) {
        $("#displayCart").click();          // Nevidljivo dugme
    })

    $('#add').prop("disabled", true);

    loadData(false);

    loadCart()


});

function loadData(provera) {
    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/items",
        success: function (data) {
            tableItems = [];
            for (var x = 0; x < data.length; x++) {
                tableItems.push(data[x])
            }
            if (provera) {
                table.destroy();
            }
            makeTable(provera);
        }
    });
}

function loadCart() {
    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "itemsCart/" + currentUser["id"],
        success: function (data) {

            cartItems = [];


            $('.table tbody').empty();
            $('.badge').text(data.length);
            if (!data.length) {
                $("#checkout").css("display", "none");
            }
            else {
                $("#checkout").css("display", "block");
            }

            for (var x = 0; x < data.length; x++) {
                cartItems.push(data[x]);
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td>' + data[x]["name"] + '</td>';
                cols += '<td>' + data[x]["category"] + '</td>';
                cols += '<td><input id="broj' + x + '" onchange="novaVrednost(' + x + ')" type="number" min="1" max="9" value="' + data[x]["quantity"] + '"></input></td>';
                cols += '<td>' + data[x]["description"] + '</td>';
                cols += '<td> <a onclick="brisi(' + x + ')" class="btn btn-xs btn-danger my-product-remove" style=" color: white; background:rgb(201,48,44);padding: 1px 5px;contrast :5.33"> X </a></td>'

                newRow.append(cols);
                $(".table").append(newRow);
            }

        }
    });

}
