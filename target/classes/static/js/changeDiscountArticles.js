var currentUser = localStorage.getItem('loggedUser');
currentUser = JSON.parse(currentUser);

var table;
var dataTable = [];


function makeTable(provera) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable
            ,
            'columns': [
                {"data": "id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "quantity"},
                {"data": "category"},
                {"data": "discount"}
            ]

        });

    if (provera){
        return
    }


    $('#datatable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            console.log("prvi");

            $('#add').prop("disabled",true);

            $(this).removeClass('selected');
        }
        else {
            console.log("drugi");
            $('#add').prop("disabled",false);

            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');


        }
    } );


    table.draw();

}

$(document).ready(function () {


    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });

    $('#add').click(function () {

        var ko = table.row('.selected')["0"];
        var selekt = dataTable[ko];
        console.log("Selekt: " + selekt["id"])

        $.ajax({
            type: "put",
            contentType: 'application/json',
            url: "updateDisc/"+ selekt["id"],
            success: function (data) {

                table.destroy();
                loadData(true);

            }

        });
    });

    $('#add').prop("disabled",true);

    loadData(false)


});

/*
*
* */

function loadData(provera)
{

    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/items",
        success: function (data) {
            dataTable = [];
            for(var i = 0 ; i < data.length;i++)
            {
                dataTable.push(
                    {
                        "id" : data[i]["id"],
                        "name" : data[i]["name"],
                        "description" : data[i]["description"],
                        "price" : data[i]["price"],
                        "quantity" : data[i]["quantity"],
                        "discount" : data[i]["discount"],
                        "category" : data[i]["category"]
                    }
                );
            }
            makeTable(provera);
        }
    });




}
