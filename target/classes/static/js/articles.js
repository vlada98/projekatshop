var table;
var dataTable = [];
var cats = [];
var indexZaEdit = -1;

function makeTable(provera) {

    table = $('#datatable').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable
            ,
            'columns': [
                {"data": "id"},
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "quantity"},
                {"data": "category"}
            ]

        });

    if (provera) return;

    $('#datatable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            console.log("prvi");

            $('#edit').prop("disabled", true);
            $('#obrisi').prop("disabled", true);

            $(this).removeClass('selected');
        }
        else {
            console.log("drugi");
            $('#edit').prop("disabled", false);
            $('#obrisi').prop("disabled", false);

            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });


    console.log("ALO");
    table.draw();

}

$(document).ready(function () {

    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });


    $('#edit').prop("disabled", true);
    $('#obrisi').prop("disabled", true);

    $('#obrisi').click(function () {
        $('#edit').prop("disabled", true);
        // table.row('.selected').remove().draw( false );
        var indexZaBrisanje = table.row('.selected')["0"];

        console.log(dataTable[indexZaBrisanje]["id"])

        $.ajax({
            type: "delete",
            url: "/deleteItem/" + dataTable[indexZaBrisanje]["id"],
            success: function (d) {
                table.destroy()
                loadData(true);
            }
        });


    });

    $('#edit').click(function () {
        $('#obrisi').prop("disabled", true);
        $("#okiniEdit").click();
        var da = table.row('.selected')["0"];
        indexZaEdit = dataTable[da]["id"];


        $("#categoryU").val(dataTable[da]["category"]),
            $("#descriptionU").val(dataTable[da]["description"]),
            $("#nameU").val(dataTable[da]["name"]),
            $("#priceU").val(dataTable[da]["price"]),
            $("#quantityU").val(dataTable[da]["quantity"])

    });

    $('#add').click(function () {
        $("#okiniAdd").click();
    });


    $('#update').click(function () {

        $.ajax({
            type: "put",
            contentType: 'application/json',
            url: "/updateItem",
            data: makeDataU(),
            success: function (data) {
                table.destroy();
                loadData(true);
            },
        });

    });


    $('#save').click(function () {

        $.ajax({
            type: "post",
            dataType: 'json',
            contentType: 'application/json',
            url: "/saveItem",
            data: makeData(),
            success: function (data) {
                table.destroy();
                loadData(true);
            },
        });

    });

    loadData(false);


});

function loadData(provera) {

    $.ajax({
        type: "get",
        contentType: 'application/json',
        url: "/items",
        success: function (data) {
            dataTable = [];
            for (var i = 0; i < data.length; i++) {
                dataTable.push(
                    {
                        "id": data[i]["id"],
                        "name": data[i]["name"],
                        "description": data[i]["description"],
                        "price": data[i]["price"],
                        "quantity": data[i]["quantity"],
                        "discount": data[i]["discount"],
                        "category": data[i]["category"]
                    }
                );
            }

            cats = [];
            for (var x = 0; x < data.length; x++) {
                if (!cats.includes(data[x]["category"])) {
                    cats.push(data[x]["category"]);
                }

            }
            $.each(cats, function (i, item) {
                $('#category').append($('<option>', {
                    value: item,
                    text: item
                }));
            });
            $.each(cats, function (i, item) {
                $('#categoryU').append($('<option>', {
                    value: item,
                    text: item
                }));
            });
            makeTable(provera);
        }
    });


}

function makeData() {
    var obj = {


        "category": $("#category").val(),
        "description": $("#description").val(),
        "name": $("#name").val(),
        "price": $("#price").val(),
        "quantity": $("#quantity").val()

    };
    return JSON.stringify(obj);
}

function makeDataU() {
    var obj = {

        "id": indexZaEdit,
        "category": $("#categoryU").val(),
        "description": $("#descriptionU").val(),
        "name": $("#nameU").val(),
        "price": $("#priceU").val(),
        "quantity": $("#quantityU").val()

    };
    return JSON.stringify(obj);
}

