var table1;
var table2;
var table3;

var dataTable1 = [];
var dataTable2 = [];
var dataTable3 = [];
$(document).ready(function () {


    $("#datepicker1").datepicker();
    $("#datepicker2").datepicker();
    $("#datepicker3").datepicker();


    $("#datepicker1").change(function () {
        console.log("value");

        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "/dayReport",
            data: JSON.stringify(
                {
                    "datum": $("#datepicker1").val()
                }
            ),
            success: function (data) {
                dataTable1 = [];
                var zarada = 0;
                var numCanc = 0;
                for (var x = 0; x < data.length; x++) {

                    if (data[x]["status"] === "CANCELED") {
                        numCanc++;
                    }
                    for (var i = 0; i < data[x]["items"].length; i++) {
                        zarada += (data[x]["items"][i]["price"] * data[x]["items"][i]["quantity"]);
                        console.log(data[x]["items"][i]["price"] + " " +  data[x]["items"][i]["quantity"])
                    }

                    dataTable1.push(
                        {
                            "date": data[x]["date"],
                            "status": data[x]["status"],
                            "sup": data[x]["supplierFirstLastName"],
                            "quantity": data[x]["quantities"].length
                        }
                    )
                }
                $("#total1").val(zarada);
                $("#noc1").val(numCanc);
                table1.destroy();
                loadTable1();

            }

        });
    });

    $("#datepicker2").change(function () {
        console.log("value");


        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "/weekReport",
            data: JSON.stringify(
                {
                    "datum": $("#datepicker2").val()
                }
            ),
            success: function (data) {
                dataTable2 = [];
                var zarada = 0;
                var numCanc = 0;
                for (var x = 0; x < data.length; x++) {

                    if (data[x]["status"] === "CANCELED") {
                        numCanc++;
                    }
                    for (var i = 0; i < data[x]["items"].length; i++) {
                        zarada += (data[x]["items"][i]["price"] * data[x]["items"][i]["quantity"]);
                        console.log(data[x]["items"][i]["price"] + " " +  data[x]["items"][i]["quantity"])
                    }

                    dataTable2.push(
                        {
                            "date": data[x]["date"],
                            "status": data[x]["status"],
                            "sup": data[x]["supplierFirstLastName"],
                            "quantity": data[x]["quantities"].length
                        }
                    )
                }
                $("#total2").val(zarada);
                $("#noc2").val(numCanc);
                table2.destroy();
                loadTable2();

            }

        });
    });

    $("#datepicker3").change(function () {
        console.log("value");


        $.ajax({
            type: "post",
            contentType: 'application/json',
            url: "/monthReport",
            data: JSON.stringify(
                {
                    "datum": $("#datepicker3").val()
                }
            ),
            success: function (data) {
                dataTable1 = [];
                var zarada = 0;
                var numCanc = 0;
                for (var x = 0; x < data.length; x++) {

                    if (data[x]["status"] === "CANCELED") {
                        numCanc++;
                    }
                    for (var i = 0; i < data[x]["items"].length; i++) {
                        zarada += (data[x]["items"][i]["price"] * data[x]["items"][i]["quantity"]);
                        console.log(data[x]["items"][i]["price"] + " " +  data[x]["items"][i]["quantity"])
                    }

                    dataTable3.push(
                        {
                            "date": data[x]["date"],
                            "status": data[x]["status"],
                            "sup": data[x]["supplierFirstLastName"],
                            "quantity": data[x]["quantities"].length
                        }
                    )
                }
                $("#total3").val(zarada);
                $("#noc3").val(numCanc);
                table3.destroy();
                loadTable3();

            }

        });
    });

    $("#logout").click(function (event) {
        event.preventDefault();
        localStorage.clear();
        window.location.href = "/login.html"
    });
    table1 = $('#datatable1').DataTable({"sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>"});
    table2 = $('#datatable2').DataTable({"sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>"});
    table3 = $('#datatable3').DataTable({"sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>"});


});


function loadTable1() {
    table1 = $('#datatable1').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable1,
            'columns': [
                {"data": "date"},
                {"data": "status"},
                {"data": "sup"},
                {"data": "quantity"}
            ]

        });
    table1.draw();
}

function loadTable2() {
    table2 = $('#datatable2').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable2,
            'columns': [
                {"data": "date"},
                {"data": "status"},
                {"data": "sup"},
                {"data": "quantity"}
            ]

        });
    table2.draw();
}

function loadTable3() {
    table3 = $('#datatable3').DataTable(
        {
            "sDom": "<'row-fluid'<'span6'T><'span6'>r>t<'row-fluid'<'span6'i><'span6'p>>", // ugasi defaultni search
            "aaData": dataTable3,
            'columns': [
                {"data": "date"},
                {"data": "status"},
                {"data": "sup"},
                {"data": "quantity"}
            ]

        });
    table3.draw();
}
